from django.db import models

# Create your models here.

class Task(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=400)
    priority = models.IntegerField(default=1)




