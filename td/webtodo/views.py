from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
# CONTROLLER

from .models import Task

def home_old(r):
    tasks = Task.objects.order_by('-priority')
    list_task = ", ".join([task.name for task in tasks])
    return HttpResponse(list_task)



def home(r):
    tasks = Task.objects.order_by('-priority')
    dictionary = {'tasks': tasks}
    return render(r, 'webtodo/index.html', dictionary)